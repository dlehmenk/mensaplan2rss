#!/usr/bin/env python3
import argparse
import logging

import datetime

import dateutil.tz
from babel.dates import format_date

import feedgen
from feedgen.feed import FeedGenerator
import requests
from bs4 import BeautifulSoup, element

from typing import Dict, List


def parse_cli_args() -> argparse.Namespace:
    p: argparse.ArgumentParser = argparse.ArgumentParser()

    p.add_argument('--url-mensa-x', help="URL zum Mensaplan für die Mensa im X Gebäude", type=str,
                   default='https://www.studierendenwerk-bielefeld.de/essen-trinken/speiseplan/bielefeld/mensa-x/')

    p.add_argument('--day', help="Datum der aktuellen Woche, für das ein Eintrag im Feed generiert"
                                 "werden soll. Kann mehrmals verwendet werden. Format: YYYYMMDD (z.B. 20230922)",
                   default=None, action='append')

    p.add_argument('--rss-file', help="Datei, in der der Feed gespeichert wird", default='feed.rss')

    p.add_argument('--feed-link', help="Link zu mehr Infos zum Feed",
                   default='https://www.studierendenwerk-bielefeld.de/essen-trinken/')
    p.add_argument('--feed-title', help="Feed Titel", default='Mensaplan Uni Bielefeld')
    p.add_argument('--feed-description', help="Feed Beschreibung", default='Mensaplan Uni Bielefeld')
    p.add_argument('--feed-icon-url', help="Feed Icon URL",
                   default='https://www.studierendenwerk-bielefeld.de/favicon.ico')
    p.add_argument('--debug', action='store_true')

    return p.parse_args()


def parse_mensa_x(url: str, days: List[str]) -> Dict[str, Dict[str, Dict[str, Dict[str, str]]]]:
    """
    Parse meal plan for canteen in the X building.
    
    :param days: days to look up
    :param url: URL to the site with the meal plan
    :return: dict of all menus by day of week
    """

    # fetch the HTML of the site
    raw_html = requests.get(url).content
    raw_site = BeautifulSoup(raw_html, features="html.parser")

    # get div with menu in it
    menu_div = raw_site.find(id='menuContainer')

    # dict to store menus in
    menus = {}

    # get menu for every day
    for day in days:
        # The day is stored as attribute 'data-selector'
        full_day = menu_div.find(attrs={"class": "menuDay", "data-selector": day})

        # convert date to german weekday name
        menus[day] = {'name': format_date(datetime.date.fromisoformat(day), 'EEEE', locale='de_DE'), 'menus': {}}

        # Sometimes the Studierendenwerk communicates changes in a not otherwise marked <strong> element
        notice = full_day.find(attrs={"class": "menuDay__headline"}).find('strong')
        if notice:
            logging.debug(f"Found notice: {notice}")
            menus[day]['menus'].update({notice: {
                'line': 'notice',
                'dish': notice.text.strip(),
                'type': 'notice',
                'labels': None,
                'allergens': None,
                'price': None}})

        # Read all menus of this day and convert them to plaintext
        menu: element.Tag
        for menu in full_day.find_all('div', {'class': 'menuItem--mainmenu'}):

            menu_info = menu.find('div', {'class': 'modal-body'})

            menu_name = menu_info.find('span', {'class': 'menuItem__line'}).text.strip()
            logging.debug(f"Menu name: {menu_name}")

            dish = " ".join(menu_info.find('strong', {'class': 'menuItem__headline'}).get_text(strip=True).split())
            logging.debug(f"\n\n{dish}")

            try:
                allergen_tags = menu_info.find('div', {'class': 'menuItem__allergens'}).find_all('dfn')
                allergens = ", ".join([t.text for t in allergen_tags])
            except AttributeError:
                allergens = ''

            price_tags = menu_info.find('div', {'class': 'menuItem__price'}).find_all('p')
            price = ", ".join([t.get_text(' ', strip=True) for t in price_tags[1:]])

            label_tags = menu_info.find_all('figcaption', {'class': 'menuItem__marking__label'})
            labels = ", ".join([t.get_text(strip=True) for t in label_tags])

            # add the dish to the dict of dishes of today
            menus[day]['menus'].update({menu_name: {
                'line': menu_name,
                'dish': dish,
                'type': 'main',
                'labels': labels,
                'allergens': allergens,
                'price': price}})

        # side dishes
        side: element.Tag
        for side in full_day.find_all('div', {'class': 'menuItem--sidedish'}):

            side_name = side.find('h3', {'class': 'menuItem__headline'}).get_text(strip=True)

            side_dish_tags = side.find_all('li', {'class': 'menuItem__sidedish'})

            sides = []

            side_dish: element.Tag
            for side_dish in side_dish_tags:

                dish = side_dish.strong.text

                # wtf studiwerk.. extract html from 'data-bs-content' attribute and parse again
                try:
                    hrghmpf = BeautifulSoup(side_dish.a['data-bs-content'], features="html.parser").find(
                        'strong', {'class': 'custombadge__code'}).get_text(", ", strip=True)
                    sides.append(f"{dish} ({hrghmpf})")
                except AttributeError:
                    sides.append(f"{dish}")

            price_tags = side.find('div', {'class': 'menuItem__price'}).find_all('p')
            price = ", ".join([t.get_text(' ', strip=True) for t in price_tags[1:]])

            menus[day]['menus'].update({side_name: {
                'line': side_name,
                'dish': ", ".join(sides),
                'type': 'side',
                'labels': None,
                'allergens': None,
                'price': price}})

    return menus


def generate_rss(menus: Dict[str, Dict[str, Dict[str, Dict[str, str]]]], title, description, link, icon_url)\
        -> feedgen.feed:
    """
    Generate a RSS feed for the menus.

    :param menus: Dict of menus to put into the feed
    :param title: Feed title
    :param description: Feed description
    :param link:  Link to homepage (Studierendenwerk)
    :param icon_url: Link to feed icon
    :return: Feed object
    """

    menufeed = FeedGenerator()
    menufeed.title(title)
    menufeed.description(description)
    menufeed.link(href=link)
    menufeed.icon(icon_url)
    menufeed.id(link)

    # Add each available day to the feed
    for day in menus:

        menus_today = []

        # Format the output
        for menu in menus[day]['menus']:
            if menus[day]['menus'][menu]['type'] == 'notice':
                menus_today.append(f"<strong>Hinweis</strong>: {menus[day]['menus'][menu]['dish']} <br/>")
            elif menus[day]['menus'][menu]['type'] == 'main':
                menus_today.append(f"<strong>{menu}</strong>: {menus[day]['menus'][menu]['dish']} <br/>"
                                   f"<i>{menus[day]['menus'][menu]['labels']}</i><br/>"
                                   f"<small>Allergene: {menus[day]['menus'][menu]['allergens']} </small><br/>"
                                   f"Preis: {menus[day]['menus'][menu]['price']}")
            else:
                menus_today.append(f"<strong>{menu}</strong>: {menus[day]['menus'][menu]['dish']} <br/>"
                                   f"Preis: {menus[day]['menus'][menu]['price']}")

        # Create the feed entry
        entry = menufeed.add_entry()
        entry.title(f"Mensaplan für {menus[day]['name']}, "
                    f"{format_date(datetime.date.fromisoformat(day), 'd.M.Y', locale='de_DE')}")
        entry.id(day)
        entry.guid(day)
        entry.description('\n \n'.join(menus_today))
        entry.content('\n \n'.join(menus_today))
        entry.published(datetime.datetime.now(dateutil.tz.gettz()))
        entry.link(href=link)

    return menufeed


def main():
    args = parse_cli_args()

    if args.debug:
        logging.basicConfig(level=logging.DEBUG, format="[%(levelname)s] %(message)s")
    else:
        logging.basicConfig(level=logging.INFO, format="[%(levelname)s] %(message)s")
  
    days = args.day

    if days is None:
        days = [datetime.datetime.today().strftime('%Y%m%d')]

    menus_mensa_x: Dict[str, Dict[str, Dict[str, Dict[str, str]]]] = parse_mensa_x(args.url_mensa_x, days)

    feed_mensa_x: feedgen.feed = generate_rss(menus_mensa_x,
                                              args.feed_title, args.feed_description,
                                              args.feed_link, args.feed_icon_url)

    feed_mensa_x.rss_file(args.rss_file)


if __name__ == "__main__":
    main()
